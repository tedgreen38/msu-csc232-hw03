/*
 * @file WordParser.cpp
 * @authors James R. Daehn and Ted Green
 * @brief Implementation of WordParser class.
 
 */

#include "WordParser.h"
 /**
   * Determine whether a given word matches the language described above.
		<word> = <dot>|<dash><word>|<word><dot>
		<dot>  = .
		<dash> = -
   *
   * @param s the string under interrogation
   * @return True is returned if the given string matches the language; false otherwise.
   */
bool WordParser::isWord(std::string s) {
	/*base case*/
	if(s.length() == 1){
		if(s[0] == DOT){
			return true;
		}
		else{
			return false;
		}
	}
	else if(s[1] == DASH and s == s.substr(1)){
		return isWord(s.substr(s.length() - 1));
	}
	else{
		return false;
	}
}
