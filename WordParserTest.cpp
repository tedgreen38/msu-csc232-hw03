/*
 * @file WordParserTest.cpp
 * @author James R. Daehn
 * @brief WordParser unit test implementation. DO NOT MODIFY THIS FILE! DOING SO
 * WILL RESULT IN A FAILING GRADE OF 0.
 */

#include "WordParserTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(WordParserTest);

const std::string WordParserTest::BASE_CASE = ".";
const std::string WordParserTest::DOT_DOT_DOT = "...";
const std::string WordParserTest::DOT_DOT_DASH = "..-";
const std::string WordParserTest::DOT_DASH_DOT = ".-.";
const std::string WordParserTest::DOT_DASH_DASH = ".--";
const std::string WordParserTest::DASH_DOT_DOT = "-..";
const std::string WordParserTest::DASH_DOT_DASH = "-.-";
const std::string WordParserTest::DASH_DASH_DOT = "--.";
const std::string WordParserTest::DASH_DASH_DASH = "---";
const std::string WordParserTest::FOUR_DOT_TWO_DASH = "....--";
const std::string WordParserTest::SEVEN_CHAR_MORE_DASH_THAN_DOTS = "------.";
const std::string WordParserTest::TEST_ASSERTION_FAILURE_MESSAGE =
		"actual should equal expected";

WordParserTest::WordParserTest() {

}

WordParserTest::~WordParserTest() {

}

void WordParserTest::setUp() {

}

void WordParserTest::tearDown() {

}

/* . */
void WordParserTest::testBaseCase() {
	bool expected = true;
	bool actual = wordParser.isWord(BASE_CASE);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* ... */
void WordParserTest::testDotDotDot() {
	bool expected = true;
	bool actual = wordParser.isWord(DOT_DOT_DOT);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* ..- */
void WordParserTest::testDotDotDash() {
	bool expected = false;
	bool actual = wordParser.isWord(DOT_DOT_DASH);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* .-. */
void WordParserTest::testDotDashDot() {
	bool expected = false;
	bool actual = wordParser.isWord(DOT_DASH_DOT);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* .-- */
void WordParserTest::testDotDashDash() {
	bool expected = false;
	bool actual = wordParser.isWord(DOT_DASH_DASH);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* -.. */
void WordParserTest::testDashDotDot() {
	bool expected = true;
	bool actual = wordParser.isWord(DASH_DOT_DOT);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* -.- */
void WordParserTest::testDashDotDash() {
	bool expected = false;
	bool actual = wordParser.isWord(DASH_DOT_DASH);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* --. */
void WordParserTest::testDashDashDot() {
	bool expected = true;
	bool actual = wordParser.isWord(DASH_DASH_DOT);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* --- */
void WordParserTest::testDashDashDash() {
	bool expected = false;
	bool actual = wordParser.isWord(DASH_DASH_DASH);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* ....-- */
void WordParserTest::testFourDotTwoDash() {
	bool expected = false;
	bool actual = wordParser.isWord(FOUR_DOT_TWO_DASH);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}

/* -------. */
void WordParserTest::testSevenCharMoreDashThanDots() {
	bool expected = true;
	bool actual = wordParser.isWord(SEVEN_CHAR_MORE_DASH_THAN_DOTS);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(TEST_ASSERTION_FAILURE_MESSAGE, expected,
			actual);
}
